from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index),
    path('register', views.register),
    path('login', views.login),
    path('main_page', views.main_page),
    path('logout', views.logout),
    path('add_quote', views.add_quote),
    path('quote/cast_like/<int:id>', views.like),
    path('process_edit/<int:user_id>', views.process_edit_user),
    path('quote/destroy/<int:id>', views.destroy_quote),
    path('user_profile/<int:id>', views.profile)
]