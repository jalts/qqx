from django.shortcuts import render, HttpResponse, redirect
from django.contrib import messages
from .models import *
import bcrypt
from django.db.models import Count

def index(request):
    # return HttpResponse("you did")
    return render(request, "index.html")

def register(request):
    if request.method == "POST":
        errors = User.objects.create_validator(request.POST)
        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/')
        else:
            hashed_pw = bcrypt.hashpw(request.POST['password'].encode(), bcrypt.gensalt()).decode()
            print(hashed_pw)
            user = User.objects.create(first_name=request.POST['first_name'], last_name=request.POST['last_name'], email=request.POST['email'], password=hashed_pw)
            request.session['user_id'] = user.id
            return redirect('/main_page')
    return redirect('/')

def login(request):
    user = User.objects.filter(email=request.POST['email'])
    if len(user) > 0:
        user = user[0]
        if bcrypt.checkpw(request.POST['password'].encode(), user.password.encode()):
            request.session['user_id'] = user.id
            return redirect('/main_page')
    messages.error(request, "email or pass is incorrect")
    return redirect('/')

def main_page(request):
    if 'user_id' not in request.session:
        messages.error(request, "you need to register or login!")
        return redirect('/')
    context = {
        'user': User.objects.get(id=request.session['user_id']),
        'all_quotes': Quote.objects.all()
    }
    return render(request, "main_page.html", context)

def logout(request):
    request.session.clear()
    return redirect('/')

def add_quote(request):
    if 'user_id' not in request.session:
        return redirect('/')
    if request.method == "POST":
        errors = Quote.objects.create_validator(request.POST)
        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/main_page')
        else:
            new_quote = Quote.objects.create(source=request.POST['source'], description=request.POST['description'], user=User.objects.get(id=request.session['user_id']))
        return redirect('/main_page')
    return redirect('/main_page')

def like(request, id):
    if 'user_id' not in request.session:
        return redirect('/')
    if request.method == "GET":
        quote_with_id = Quote.objects.filter(id=id)
        if len(quote_with_id) > 0:
            quote = Quote.objects.get(id=id)
            user = User.objects.get(id=request.session['user_id'])
            quote.user_likes.add(user)
    return redirect('/main_page')

def profile(request, id):
    context = {
        'user': User.objects.get(id=id)
    }
    return render(request, "poster_profile.html", context)

def process_edit_user(request, user_id):
    if 'user_id' not in request.session:
        return redirect('/')
    if request.method=='POST':
        errors = User.objects.update_validator(request.POST)
        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/main_page')
        else:
            my_user = User.objects.get(id=user_id)
            my_user.first_name=request.POST['first_name']
            my_user.last_name=request.POST['last_name']
            my_user.email=request.POST['email']
            my_user.save()
            return redirect('/main_page')
    else:
        context={
            'user': User.objects.get(id=user_id)
        }
        return render(request, "profile.html", context)  
    return redirect('/main_page')


def destroy_quote(request, id):
    if 'user_id' not in request.session:
        return redirect('/')
    if request.method == "POST":
        quote_with_id = Quote.objects.filter(id=id)
        if len(quote_with_id) > 0:
            quote = quote_with_id[0]
            if quote.user.id == request.session['user_id']:
                quote.delete()
    return redirect('/main_page')





